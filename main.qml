import QtQuick 2.12
import QtQuick.Window 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12
import QtQuick.Controls.Material 2.12

ApplicationWindow {
    id: _root
    width: 1280
    height: 1024
    visibility: "FullScreen"
    visible: true
    title: qsTr("Vervangingsencryptie")
    Material.theme: Material.Light
    Material.accent: Material.Purple
    RowLayout {
        id: rowLayout
        anchors.fill: parent
        anchors.margins: 10
        spacing: 10


        GroupBox {
            title: "tekst"
            Layout.fillHeight: true

            ColumnLayout {
                anchors.fill: parent

                TextArea {
                    id: _plainText
                    Layout.fillHeight: true
                    Layout.preferredWidth: _root.width / 3
                    selectByMouse: true

                    text: "Volgens Google heeft een van zijn experimentele quantumcomputers een berekening voltooid die onmogelijk is met traditionele computers. De techgigant spreekt van een grote doorbraak.

        De bevindingen zijn gepubliceerd in het het gerenommeerde wetenschappelijke tijdschrift Nature. De berekening is als het ware een demonstratie van wat de speciale computer genaamd Sycamore kan. De prestatie wordt door wetenschappers vergeleken met de eerste vliegtuigvlucht van de Wright-broers."
                    wrapMode: TextArea.WrapAtWordBoundaryOrAnywhere
                }
            }
        }


        ColumnLayout {
            Layout.alignment: Qt.AlignTop
            GroupBox {
                Layout.alignment: Qt.AlignHCenter
                Layout.fillWidth: true
                RowLayout {
                    anchors.fill: parent
                    Label {
                        text: "Sleutel:"
                    }

                    TextField {
                        Layout.fillWidth: true

                        id: keyEdit
                        text: ""

                        onTextChanged: {
                            createCipherAlphabet(text)
                        }
                    }
                }
            }

            GroupBox {
                title: "alfabet"
                Layout.alignment: Qt.AlignHCenter
                Layout.fillWidth: true
                GridLayout {
                    columns: 2

                    Label {
                        text: "Van:"
                    }
                    Label {
                        id: _plainAlphabet
                        Layout.alignment: Qt.AlignHCenter
                        text: "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
                    }

                    Label {
                        text: "Naar:"
                    }
                    Label {
                        id: _cipherAlphabet
                        Layout.alignment: Qt.AlignHCenter
                        text: "ABCDEFGHIJKLMNOPQRSTUVWXYZ"

                        color: "red"
                    }
                }

            }

            RowLayout {
                Layout.alignment: Qt.AlignHCenter

                Button {
                    Layout.alignment: Qt.AlignHCenter
                    id: encryptButton
                    text: "Versleutel"

                    onClicked: {
                        _cipherText.text = substitute(_plainText.text, _plainAlphabet.text, _cipherAlphabet.text)
                    }
                }

                Button {
                    Layout.alignment: Qt.AlignHCenter
                    id: decryptButton
                    text: "Ontcijfer"

                    onClicked: {
                        _plainText.text = substitute(_cipherText.text, _cipherAlphabet.text, _plainAlphabet.text)
                    }
                }
            }
        }

        GroupBox {
            title: "geheim"
            Layout.fillHeight: true

            ColumnLayout {
                anchors.fill: parent


                TextArea {
                    id: _cipherText
                    Layout.preferredWidth: _root.width / 3
                    Layout.fillHeight: true

                    selectByMouse: true
                    wrapMode: TextArea.WrapAtWordBoundaryOrAnywhere
                }
            }
        }
    }

    function removeDuplicateCharacters(string) {
        return string
        .split('')
        .filter(function(item, pos, self) {
            return self.indexOf(item) === pos;
        })
        .join('');
    }

    function keepOnlyAlphabetCharacters(string) {
        var plainAlphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
        return string
        .split('')
        .filter(function(item, pos, self) {
            return plainAlphabet.indexOf(item) >= 0
        })
        .join('');
    }

    function createCipherAlphabet(key) {
        key = key.toUpperCase()
        key = removeDuplicateCharacters(key)
        key = keepOnlyAlphabetCharacters(key)

        var plainAlphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
        var cipherAlphabet = key

        for (var i = 0; i < plainAlphabet.length; i++) {
            var c = plainAlphabet.charAt(i)
            if (!cipherAlphabet.includes(c)) {
                cipherAlphabet = cipherAlphabet + c
            }
        }
        _cipherAlphabet.text = cipherAlphabet
    }

    function substitute(text, sourceAlphabet, targetAlphabet) {
        text = text.toUpperCase()
        var result = ""
        for (var i = 0; i < text.length; i++) {
            var p =  text.charAt(i)
            var index = sourceAlphabet.indexOf(p)
            if (index >= 0) {
                result += targetAlphabet.charAt(index)
            } else {
                result += p
            }
        }
        return result
    }
}
